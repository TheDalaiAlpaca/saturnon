# saturnon


## Description

saturnon is a flexible treeview based TUI file system navigator that only expands the directory you are visiting. Navigation works with arrow keys or the vim keys (h,j,k,l) by default.  Powerful functionality can be added by creating specialized config files. See the examples below.  

![treeview example](saturnon.png)

## Features

- Change keybindings
- Execute custom commands on the selection with custom keybindings
- Filter using a pattern
- Sort files
- Change the root of the treeview
- Change color of the selection
- Show/hide hidden files
- Use it as a file picker

## Installation

Tested on:

| Distro       | Status  |
|--------------|---------|
| Arch Linux   | Working |
| Debian 11    | Broken  |
| Ubuntu 22.10 | Working |
| Fedora 36    | Working |

### Arch Linux
The package is available as [saturnon](https://aur.archlinux.org/packages/saturnon/) in the AUR.

### Manual download

`git clone https://gitlab.com/TheDalaiAlpaca/saturnon.git`  

`cd saturnon`  

`chmod u+x saturnon`  

`./saturnon`  

## Usage as a music player

Note: you need to have `vlc` and `dbus` installed for this.  
Run saturnon with the provided example config file for music playing. If you have saturnon installed:  
`saturnon -c /etc/saturnon/music.conf`  
Or if have just cloned the repo:  
`./saturnon -c conf/music.conf`  
You can now play the selection by pressing enter and  
toggle between play and pause by pressing space.

## Usage as a Youtube player
  
See [this](https://gitlab.com/TheDalaiAlpaca/sat-yt/).
